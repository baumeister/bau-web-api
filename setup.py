from setuptools import setup, find_packages

setup(
    name = "beautifierapi",
    version = "0.1",
    author = "Efe Surekli",
    author_email = "efe@baumeister-ai.com",
    description = ("Beautifier API for Baumeister AI"),
    package_dir={'': 'src'},
    install_requires=[
        'flask',
        'beautifulsoup4',
        'requests',
        'beautifier@git+ssh://git@bitbucket.org/baumeister/beautifier.git@master'
    ],
    packages=find_packages(where='src', exclude=['tests'])
)